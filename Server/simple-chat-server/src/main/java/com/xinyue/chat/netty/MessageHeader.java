package com.xinyue.chat.netty;

public class MessageHeader {

	public static int HeaderLength = 29;
	private int messageLength;  //消息 长度
	private int messageId;      //消息 id
	private int seqId;          //消息 序列id
	private long clientSendTime; 
	private long serverSendTime;
	private int messageType;//消息类型，1 request-response,2,server push ,3 是心跳消息
	
	
	public int getMessageType() {
		return messageType;
	}
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
	public int getMessageLength() {
		return messageLength;
	}
	public void setMessageLength(int messageLength) {
		this.messageLength = messageLength;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public int getSeqId() {
		return seqId;
	}
	public void setSeqId(int seqId) {
		this.seqId = seqId;
	}
	public long getClientSendTime() {
		return clientSendTime;
	}
	public void setClientSendTime(long clientSendTime) {
		this.clientSendTime = clientSendTime;
	}
	public long getServerSendTime() {
		return serverSendTime;
	}
	public void setServerSendTime(long serverSendTime) {
		this.serverSendTime = serverSendTime;
	}
	
	
}
