package com.xinyue.chat.netty;

import com.alibaba.fastjson.JSONObject;
/**
 * 网络能信消息结构
 * @author wgs
 *
 */
public class MessageRequest {
	
	private MessageHeader header;
	private JSONObject body;
	public MessageHeader getHeader() {
		return header;
	}
	public void setHeader(MessageHeader header) {
		this.header = header;
	}
	public JSONObject getBody() {
		return body;
	}
	public void setBody(JSONObject body) {
		this.body = body;
	}
	
	
}
