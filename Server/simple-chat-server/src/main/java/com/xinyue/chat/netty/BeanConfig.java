package com.xinyue.chat.netty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;

@Configuration
public class BeanConfig {

	@Autowired
	private NettyServer nettyServer;
	@Bean
	public ChannelGroup getChannelGroup() {
		return new DefaultChannelGroup(nettyServer.nextWorker());
	}
}
