package com.xinyue.chat.netty;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.util.concurrent.EventExecutor;

@Service
public class NettyServer implements CommandLineRunner {
	private static Logger logger = LoggerFactory.getLogger(NettyServer.class);
	private NioEventLoopGroup bossGroup = null;
	private NioEventLoopGroup workerGroup = null;
	@Autowired
	private NettyServerConfig serverConfig;
	@Autowired
	private ApplicationContext context;

	@PostConstruct
	public void init() {
		bossGroup = new NioEventLoopGroup(serverConfig.getBossWorkers());
		// 业务逻辑线程组
		workerGroup = new NioEventLoopGroup(serverConfig.getLogicWorkers());
	}

	@Override
	public void run(String... args) throws Exception {
		int port = this.serverConfig.getPort();
		try {
			ServerBootstrap b = new ServerBootstrap();
			// 这里遇到一个小问题，如果把childHandler的加入放在option的前面，option将会不生效。我用java socket连接，一直没有消息返回。
			b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class).option(ChannelOption.SO_BACKLOG, 128)
					.childOption(ChannelOption.SO_KEEPALIVE, true).childOption(ChannelOption.TCP_NODELAY, true)
					.childHandler(new ChannelInitializer<Channel>() {
						@Override
						protected void initChannel(Channel ch) throws Exception {
							ChannelPipeline p = ch.pipeline();
							p.addLast("EncodeHandler", new EncodeHandler());// 添加编码Handler
							p.addLast(new LengthFieldBasedFrameDecoder(serverConfig.getMaxBuffers(), 0, 4, -4, 0));// 添加拆包
							p.addLast("HeatbeatHandler",new HeartbeatHandler());//添加心跳处理handler
							p.addLast("DecodeHandler", new DecodeHandler());// 添加解码
							p.addLast("ChatDispatchHandler", new ChatDispatchHandler(context));
						}
					});
			logger.info("启动服务，端口:{}", serverConfig.getPort());
			ChannelFuture f = b.bind(port).sync();
			f.channel().closeFuture().sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}

	public EventExecutor nextWorker() {
		return workerGroup.next();
	}

}
