package com.xinyue.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatServerMain {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ChatServerMain.class);
		app.run(args);
	}
	

}
