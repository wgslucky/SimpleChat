package com.xinyue.chat.netty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
@Configuration
@ConfigurationProperties(prefix="netty.server.config")
public class NettyServerConfig {

	private int port = 8601;
	private int bossWorkers = 1;
	private int logicWorkers = 2;
	
	private int maxBuffers = 1024 * 8;

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getBossWorkers() {
		return bossWorkers;
	}

	public void setBossWorkers(int bossWorkers) {
		this.bossWorkers = bossWorkers;
	}

	public int getLogicWorkers() {
		return logicWorkers;
	}

	public void setLogicWorkers(int logicWorkers) {
		this.logicWorkers = logicWorkers;
	}

	public int getMaxBuffers() {
		return maxBuffers;
	}

	public void setMaxBuffers(int maxBuffers) {
		this.maxBuffers = maxBuffers;
	}
	
	
}
