package com.xinyue.chat.netty;

import com.alibaba.fastjson.JSONObject;

public class MessageResponse {

	
	private MessageHeader header;
	private JSONObject responseBody;
	
	public MessageResponse() {
		
	}
	public MessageResponse(MessageRequest request) {
		super();
		this.header = request.getHeader();
	}
	public MessageHeader getHeader() {
		return header;
	}
	public void setHeader(MessageHeader header) {
		this.header = header;
	}
	public JSONObject getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(JSONObject responseBody) {
		this.responseBody = responseBody;
	}
	
	
}
