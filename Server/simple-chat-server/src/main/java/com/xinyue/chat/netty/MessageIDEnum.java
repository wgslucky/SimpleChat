package com.xinyue.chat.netty;

/**
 * 请求消息号管理
 * 
 * @author wgs
 *
 */
public enum MessageIDEnum {
	HEARTBEAT(101,"心跳"),
	ENTER_CHAT_ROOT(1001,"进入聊天室"),
	SEND_CHAT_MESSAGE(1002,"发送聊天消息"),
	
	;
	private int messageId;
	private String desc;
	private MessageIDEnum(int messageId, String desc) {
		this.messageId = messageId;
		this.desc = desc;
	}
	public int getMessageId() {
		return messageId;
	}
	public String getDesc() {
		return desc;
	}
	
	public boolean isMatch(int messageId) {
		return this.messageId == messageId;
	}
	
	
	

}
