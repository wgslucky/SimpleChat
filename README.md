# SimpleChat

#### 项目介绍
在网络游戏开发中，客户端与服务器的网络通信是必不可少的，对于进入网络游戏开发行业的新手来说，也是一个必须攻克的难点，否则，难以向架构师发展。本示例以一个简单的聊天程序为基础，客户端使用Unity3d开发，使用的版本是Unity 2019.2.5f1 (64-bit)，服务器网络层使用Netty + Spring Boot（<version>2.1.3.RELEASE</version>）开发，IDE是Eclipse,版本是Version: 2018-12 (4.10.0)。本示例可以做为学习游戏网络长连接通信的基础案例。主要实现的功能有：
- C# socket 长连接
- 网络通信
- 客户端异步发送消息
- 客户端异步接收消息
- 客户端断包，粘包处理
- 客户端消息编码与解码
- 客户端网络状态管理
- 心跳管理
- Netty服务启动
- Netty消息接收与返回
- Netty编码与解码实现
- Netty断包，粘包处理
- Netty多客户端连接管理
- Netty向多个客户端广播消息

* 运行预览

![聊天界面](https://images.gitee.com/uploads/images/2019/1117/153141_368525b5_23677.png "屏幕截图.png")

* 程序示例下载: [http://www.xinyues.com/h-nd-195.html#_np=2_627](http://www.xinyues.com/h-nd-195.html#_np=2_627)

### 关注公众号，获取更多的开发文档
![输入图片说明](https://images.gitee.com/uploads/images/2019/1104/225039_95a53e40_23677.png "QQ截图20191104223446.png")