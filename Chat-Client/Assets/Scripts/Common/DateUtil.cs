﻿using UnityEngine;
using System.Collections;
using System;

public class DateUtil 
{

   public static long CurrentMillTime()
    {
        TimeSpan tss = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
        long millTime = Convert.ToInt64(tss.TotalMilliseconds);
        return millTime;

    }
    public static long CurrentSecond()
    {
        return CurrentMillTime() / 1000;
    }
}
