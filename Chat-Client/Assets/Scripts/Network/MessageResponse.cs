﻿using UnityEngine;
using System.Collections;

public class MessageResponse 
{

    private MessageHeader header;
    private string body;

    public MessageHeader Header { get => header; set => header = value; }
    public string Body { get => body; set => body = value; }
}
