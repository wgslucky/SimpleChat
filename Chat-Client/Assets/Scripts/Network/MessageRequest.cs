﻿using UnityEngine;
using System.Collections;

public class MessageRequest 
{
    private MessageHeader header;
    private object body;
    private OnResponseMessageHandler responseHandler;


    public MessageHeader Header { get => header; set => header = value; }
    public object Body { get => body; set => body = value; }
    public OnResponseMessageHandler ResponseHandler { get => responseHandler; set => responseHandler = value; }
}
