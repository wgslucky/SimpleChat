﻿using UnityEngine;
using System.Collections;

public class MessageHeader 
{
    public const int HeaderLength = 29;
    private int messageLength;
    private int seqId;
    private int messageId;
    private long clientSendTime;
    private long serverSendTime;

    private int messageType;// 1 request-response消息，2 服务器push消息。3 是心跳消息

    public int MessageLength { get => messageLength; set => messageLength = value; }
    public int SeqId { get => seqId; set => seqId = value; }
    public int MessageId { get => messageId; set => messageId = value; }
    public long ClientSendTime { get => clientSendTime; set => clientSendTime = value; }
    public long ServerSendTime { get => serverSendTime; set => serverSendTime = value; }
    public  int MessageType { get => messageType; set => messageType = value; }
}
