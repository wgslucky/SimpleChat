﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// 心跳管理,如果客户端长时间未向服务器发送业务消息 ，则客户端主动向服务器定时发送一条心跳消息 
/// </summary>
public class HeartbeatManager
{
    private readonly XinyueSocketClient socketClient;
    private const int HeartTime = 5; //心跳每5秒检测一次，单位秒
    private long lastReceieveMessageTime = DateUtil.CurrentSecond();//最后一次接收心跳返回的时间
    private int heartbeatCount;//心跳发送的次数
    private volatile ConnectedStatus connectedStatus;

    public HeartbeatManager(XinyueSocketClient socketClient,ReadSocketDataManager readSocketDataManager)
    {
        this.socketClient = socketClient;
        readSocketDataManager.ReadSocketDataEventHandler += OnReadData;
        socketClient.CompleteConnectServerHandler += (connectedStatus) =>
        {
            this.connectedStatus = connectedStatus;
        };
    }

    public void Update()
    {
        if(this.connectedStatus != ConnectedStatus.ConnectedSucess)
        {
            return;
        }
        long nowTime = DateUtil.CurrentSecond();
        float intervalTime = nowTime - lastReceieveMessageTime;
        if (intervalTime > HeartTime)
        {
            this.SendHeartbeatMesage();
            heartbeatCount++;
           if(heartbeatCount * HeartTime >= 3600)
            {
                Debug.Log("心跳时间太长了，估计玩家已经不操作了");
                
            }
        }
        if(intervalTime > HeartTime * 600)
        {
            //心跳长时间未返回，说明服务器已经失联了，断开连接吧
            Debug.Log("由于长时间未操作客户端，主动断开连接");
            socketClient.Close();
        }
    }
    private void SendHeartbeatMesage()
    {
        int totalSize = 8;
        ByteBuf byteBuf = new ByteBuf(totalSize);
        byteBuf.WriteInt(totalSize);
        byteBuf.WriteInt((int)MessageIDEnum.Heartbeat);
        socketClient.SendToServer(byteBuf.ToArray());
    }
    private void OnReadData(ByteBuf byteBuf)
    {
        this.lastReceieveMessageTime = DateUtil.CurrentSecond();
        if (!byteBuf.IsReadable())
        {
            return;
        }
        
        byteBuf.MarkReaderIndex();
        byteBuf.ReadInt();
        int messageId = byteBuf.ReadInt();
        //如果是心跳消息，在这里处理，否则放过给别的模块处理
        if (messageId == (int)MessageIDEnum.Heartbeat)
        {
           Debug.Log("system:收到心跳消息");
        } else
        {
            this.heartbeatCount = 0;
            byteBuf.ResetReaderIndex();
        }
    }

}
