﻿using UnityEngine;
using UnityEditor;

public enum MessageIDEnum 
{
    Heartbeat = 101,//心跳消息
    EnterChatRoom = 1001,
    ChatMessage = 1002
    
}