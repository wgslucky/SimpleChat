﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    private static Player instance;

    public static Player GetInstance()
    {
        return instance;
    }
    private string nickName;

    public string NickName { get => nickName; set => nickName = value; }

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
