﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NicknamePanel : MonoBehaviour
{
    public InputField nickNameInput;
    public GameObject startPanel;
    public GameObject tipPanel;
    public GameObject chatPanel;
    public Text tipText;
    public Text netText;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnStartBtnClick()
    {
        string nickName = nickNameInput.text;
        if (nickName == null || nickName.Equals(""))
        {
            tipText.text = "昵称不能为空";
            return;
        }
        Player player = Player.GetInstance();
        player.NickName = nickName;
        startPanel.SetActive(false);
        tipPanel.SetActive(true);
        //连接服务器
        XinyueGameClient clientManager = XinyueGameClient.Instance();
        clientManager.ConnectEventHandler += OnConnectedSuccess;
        clientManager.StartConnectServer();

    }
    public void OnConnectedSuccess(ConnectedStatus status)
    {
        if(status == ConnectedStatus.ConnectedSucess)
        {
            XinyueGameClient clientManager = XinyueGameClient.Instance();
            clientManager.ConnectEventHandler -= OnConnectedSuccess;
            netText.text = "服务器连接成功";
            tipPanel.SetActive(false);
            chatPanel.SetActive(true);
            EnterChatRoom();
        }
       

    }
    public void EnterChatRoom()
    {
        //加入聊天室
        XinyueGameClient gameClient = XinyueGameClient.Instance();
        gameClient.RequestMessageManager.SendRequest(MessageIDEnum.EnterChatRoom, Player.GetInstance().NickName, (response => {
            ChatManager.GetInstance().EnterChatRoom(response.Body);
        }));
    }
    
    public void OnInputChange()
    {
        tipText.text = "";
    }
}
