# SimpleChat

#### Description
简单的聊天程序,客户端使用Unity3d开发，服务器使用Netty，客户端与服务端使用长连接通信，本项目只是简单实现聊天信息的发送与接收，用于阐述客户端与服务端的网络通信方式，主要解决的问题有：

* 协议制定
* 断包，粘包处理
* 消息发送与接收

<hr/>
具体的技术文档，可以关注公众号查看

![游戏技术公众号](https://images.gitee.com/uploads/images/2019/1028/144520_85b33152_23677.jpeg "qrcode_for_gh_b9070b0d04e3_258.jpg")