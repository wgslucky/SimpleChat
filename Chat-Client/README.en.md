# SimpleChat

#### Description
简单的聊天程序,客户端使用Unity3d开发，服务器使用Netty，客户端与服务端使用长连接通信，本项目只是简单实现聊天信息的发送与接收，用于阐述客户端与服务端的网络通信方式，主要解决的问题有：

* 协议制定
* 断包，粘包处理
* 消息发送与接收
